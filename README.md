# c-pipes



This program written in the C language will render random coloured
zigzag lines in the terminal, while the font, speed, density and
number of lines are fully customizable.
Each line stops once it reaches the edge of the window, only for
a new line to begin.

[![c-pipes.png](images/video.png){height=200}]( https://youtu.be/tdya6_uux_I )


This program was inspired by this bash script:

[https://github.com/pipeseroni/pipes.sh](https://github.com/pipeseroni/pipes.sh)



## USAGE

First the user has to clone the repo, then change directory to `c-pipes/`:

```
git clone https://gitlab.com/christosangel/c-pipes&&cd c-pipes
```

While in the `c-pipes/` directory,first you need to
 compile:


```
gcc c-pipes.c -Wall -o c-pipes
```

Then you can run the executable:

```
 ./c-pipes
```

You may want to add the flags that you prefer in order to have
an outcome of your liking.

## FLAGS

The user can either use the short or the long flag version
(i.e. c-pipes -h and c-pipes --help are the same).


|n|Short flag|Long flag|Explanation|Acceptable values|Default value|
|----|-------|---|----|----|---|
|1|`-h` |`--help`|	Shows this help text.|
|2|`-l` |`--lines`|	Defines the number of lines to appear at the same time	on the terminal window.| 1-9|`3`|
|3|`-s`|`--speed`|	Defines the speed of the lines.|0-9| `3`|
|4|`-d`|`--dense`|	Defines how many lines will be rendered before the screen is empty again.| 0-9|` 5`|
||||		`0` Screen refreshed after only one line|
|||	|	`9` Screen refreshed after 90 lines|
|5|`-f`|`--font`|	Defines the font of the lines.|0-9| `1`|
|||	|	`0`: Chars used:`─│┘┐└┌	`|
||||  `1`: Chars used:`─│╯╮╰╭`|
|||	|	`2`: Chars used:`━┃┛┓┗┏`|
||||	`3` Chars used:`═║╝╗╚╔`|
|||	|	`4`: Chars used:`┈┊┐┘└┌`|
||||	`5` Chars used:`┄┆┘┐└┌`|
|||	|	`6`: Chars used:`┅┇┛┓┗┏`|
||||	`7`: Chars used:`┉┋┛┓┗┏`|
|||	|	`8`: Chars used:`↑↓↗↖↘↙←→`	|
||||`9`: Chars used:`█`|
|6|`-t`|`--turn`	|Defines the probability of the lines to be straight or full of turns.| 0-9| `5`|
|||	|	`0`: lines with frequent turns.|
|||	|	`9`: lines quite straight, less turns.|
|7|`-m`|`--monochrome`|	Renders single colored lines.	If this flag is omitted, each line gets a random color.| 0-7|
||||	`0` : black
||||	`1`: red|
||||	`2` : green|
|||| `3`: yellow|
||||	`4` : blue|
||||	`5` : magenta|
||||	`6` : cyan|
||||	`7` : white|



## EXAMPLES

```
 ./c-pipes
```

will render lines with the default values:

![c-pipes](images/c-pipes.png){height=200}

```
./c-pipes -f 0 -m 2 -d 7 -l 4
```

will produce a quite dense screen with 4 rapid growing green lines with sharp angles:

![c-pipes2.pg](images/c-pipes2.png){height=200}

![c-pipes3.pg](images/c-pipes3.png){height=200} 

![c-pipes4.pg](images/c-pipes4.png){height=200}
---
Feel free to discover the endless possibilities of customization.
